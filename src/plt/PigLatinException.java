package plt;

public class PigLatinException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PigLatinException(String message) {
		super(message);
	}

}

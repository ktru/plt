package plt;

public class Translator {
	
	public static final String NIL = "nil";
	private String phrase;

	public Translator(String inputPhrase) throws PigLatinException {
		if (!inputPhrase.isEmpty() && !isValidPhrase(inputPhrase)) {
			throw new PigLatinException("Invalid phrase.");
		}
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate(){
		StringBuilder wordBuilder = new StringBuilder();
		StringBuilder phraseBuilder = new StringBuilder();
		for (char c : phrase.toCharArray()) {
			if (Character.isLetter(c)) {
				wordBuilder.append(c);
			} else {
				String translatedWord = translateWord(wordBuilder.toString());
				phraseBuilder.append(translatedWord);
				phraseBuilder.append(c);
				wordBuilder = new StringBuilder();
			}
		}
		phraseBuilder.append(translateWord(wordBuilder.toString()));
		
		return phraseBuilder.toString().isEmpty() ? NIL : phraseBuilder.toString();
	}
	
	private boolean startsWithVowel(String word) {
		return word.matches("^[aeiouAEIOU].*(?i)");
	}
	
	private boolean endsWithVowel(String word) {
		return word.matches(".*[aeiouAEIOU]$(?i)");
	}
	
	private boolean isConsonant(char c) {
		return "BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz".indexOf(c) != -1;
	}
	
	private String getInitialConsonants(String word) {
		StringBuilder bld = new StringBuilder();
		int index = 0;
		char c = word.charAt(index);
		while (isConsonant(c) && index < word.length() - 1) {
			bld.append(c);
			index++;
			c = word.charAt(index);
		}
		return bld.toString();
	}
	
	private String translateWord(String word){

		if (startsWithVowel(word)) {
			String suffix = "";
			if (word.endsWith("y")) {
				suffix = "nay";
			} else if (endsWithVowel(word)) {
				suffix = "yay";
			} else if (!endsWithVowel(word)) {
				suffix = "ay";
			}
			return word + (isUpperCase(word) ? suffix.toUpperCase() : suffix);
		} else if (!word.isEmpty() && isConsonant(word.charAt(0))) {
			String initialConsonants = getInitialConsonants(word);
			String translatedWord = word.substring(initialConsonants.length()) + initialConsonants + "ay";
			if (Character.isUpperCase(word.charAt(0))) {
				translatedWord = translatedWord.substring(0, 1).toUpperCase() + translatedWord.substring(1).toLowerCase();
			}
			return translatedWord;
		}
		return "";
	}
	
	private boolean isValidCharacter(char c) {
		return " -.,;:?!'()".indexOf(c) != -1;
	}
	
	private boolean isUpperCase(String word) {
		for (char c : word.toCharArray()) {
			if (Character.isLowerCase(c)) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isValidCase(String word) {
		return word.matches("[A-Z][a-z]*") || word.matches("[a-z][a-z]*") || word.matches("[A-Z][A-Z]*");
	}
	
	private boolean isValidPhrase(String inputPhrase) {
		for (char c : inputPhrase.toCharArray()) {
			if (!Character.isLetter(c) && !isValidCharacter(c)) {
				return false;
			}
		}
		
		String[] words = inputPhrase.split("[ .,;:?!'()-]");
		for (String word : words) {
			if (!isValidCase(word)) {
				return false;
			}
		}
		
		return true;
	}

}
